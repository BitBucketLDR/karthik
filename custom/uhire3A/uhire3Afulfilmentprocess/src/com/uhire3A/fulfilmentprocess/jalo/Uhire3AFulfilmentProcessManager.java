/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uhire3A.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.uhire3A.fulfilmentprocess.constants.Uhire3AFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class Uhire3AFulfilmentProcessManager extends GeneratedUhire3AFulfilmentProcessManager
{
	public static final Uhire3AFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Uhire3AFulfilmentProcessManager) em.getExtension(Uhire3AFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
