/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uhire3APromotions.setup;

import static com.uhire3APromotions.constants.Uhire3APromotionsConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.uhire3APromotions.constants.Uhire3APromotionsConstants;
import com.uhire3APromotions.service.Uhire3APromotionsService;


@SystemSetup(extension = Uhire3APromotionsConstants.EXTENSIONNAME)
public class Uhire3APromotionsSystemSetup
{
	private final Uhire3APromotionsService uhire3APromotionsService;

	public Uhire3APromotionsSystemSetup(final Uhire3APromotionsService uhire3APromotionsService)
	{
		this.uhire3APromotionsService = uhire3APromotionsService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		uhire3APromotionsService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return Uhire3APromotionsSystemSetup.class.getResourceAsStream("/uhire3APromotions/sap-hybris-platform.png");
	}
}
