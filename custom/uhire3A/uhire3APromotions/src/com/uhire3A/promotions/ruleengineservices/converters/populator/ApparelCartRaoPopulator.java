/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.uhire3A.promotions.ruleengineservices.converters.populator;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ruleengineservices.converters.populator.CartRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.CartRAO;

import com.uhire3A.core.user.service.ApparelUserService;
 

public class ApparelCartRaoPopulator extends CartRaoPopulator
{

	private ApparelUserService apparelUserService;


	@Override
	public void populate(final AbstractOrderModel source, final CartRAO target)
	{
		super.populate(source, target);

		target.setNewCustomer(isNewCustomer(source));
	}


	/**
	 *
	 */
	private Boolean isNewCustomer(final AbstractOrderModel source)
	{
		if (!getApparelUserService().isAnonymousUser(source.getUser()))
		{
			return getApparelUserService().isNewCustomer(source.getUser());
		}
		return Boolean.FALSE;
	}




	/**
	 * @return the apparelUserService
	 */
	public ApparelUserService getApparelUserService()
	{
		return apparelUserService;
	}

	/**
	 * @param apparelUserService
	 *           the apparelUserService to set
	 */
	public void setApparelUserService(final ApparelUserService apparelUserService)
	{
		this.apparelUserService = apparelUserService;
	}


}
