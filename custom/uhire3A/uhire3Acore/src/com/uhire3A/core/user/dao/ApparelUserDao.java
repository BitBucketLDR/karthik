/**
 *
 */
package com.uhire3A.core.user.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;


/**
 * @author user
 *
 */
public interface ApparelUserDao extends UserDao
{

	public Boolean isNewCustomer(UserModel userModel);
}