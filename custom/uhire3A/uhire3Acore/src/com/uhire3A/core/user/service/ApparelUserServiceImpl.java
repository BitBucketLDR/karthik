/**
 *
 */
package com.uhire3A.core.user.service;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;

import com.uhire3A.core.user.dao.ApparelUserDao;

/**
 * @author user
 *
 */
public class ApparelUserServiceImpl extends DefaultUserService implements ApparelUserService
{

	private ApparelUserDao apparelUserDao;

	/*
	 * @see
	 * com.uniquehire12.core.user.service.ApparelUserService#isNewCustomer(de.hybris.platform.core.model.user.UserModel)
	 */
	@Override
	public Boolean isNewCustomer(final UserModel userModel)
	{
		return getApparelUserDao().isNewCustomer(userModel);

	}

	/**
	 * @return the apparelUserDao
	 */
	public ApparelUserDao getApparelUserDao()
	{
		return apparelUserDao;
	}

	/**
	 * @param apparelUserDao
	 *           the apparelUserDao to set
	 */
	public void setApparelUserDao(final ApparelUserDao apparelUserDao)
	{
		this.apparelUserDao = apparelUserDao;
	}

}