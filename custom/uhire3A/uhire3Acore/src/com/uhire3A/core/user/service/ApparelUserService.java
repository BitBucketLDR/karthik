/**
 *
 */
package com.uhire3A.core.user.service;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

/**
 * @author user
 *
 */
public interface ApparelUserService extends UserService
{
	public Boolean isNewCustomer(UserModel userModel);

}