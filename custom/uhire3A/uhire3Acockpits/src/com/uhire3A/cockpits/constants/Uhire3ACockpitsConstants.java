/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uhire3A.cockpits.constants;

/**
 * Global class for all Uhire3ACockpits constants. You can add global constants for your extension into this class.
 */
public final class Uhire3ACockpitsConstants extends GeneratedUhire3ACockpitsConstants
{
	public static final String EXTENSIONNAME = "uhire3Acockpits";

	public static final String JASPER_REPORTS_MEDIA_FOLDER = "jasperreports";

	private Uhire3ACockpitsConstants()
	{
		// empty
	}
}
