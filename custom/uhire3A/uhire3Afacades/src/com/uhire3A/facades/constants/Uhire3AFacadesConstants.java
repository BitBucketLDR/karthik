/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uhire3A.facades.constants;

/**
 * Global class for all Uhire3AFacades constants.
 */
@SuppressWarnings("PMD")
public class Uhire3AFacadesConstants extends GeneratedUhire3AFacadesConstants
{
	public static final String EXTENSIONNAME = "uhire3Afacades";

	private Uhire3AFacadesConstants()
	{
		//empty
	}
}
